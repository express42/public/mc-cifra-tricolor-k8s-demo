# Prometheus + k8s = ❤️❤️❤️

Для начала запустим minikube следующей командой:  

```
minikube start --driver=hyperkit --cpus 8 --memory 8192
Где --cpus 8 - 8 ядер для нашей ВМ
Где --memory 8192 - 8 ГБ для нашей ВМ
```

После успешного старта нашего мини кластер в консоли вы должны увидеть следующее:  

```
😄  minikube v1.24.0 on Darwin 11.6
✨  Using the hyperkit driver based on user configuration
👍  Starting control plane node minikube in cluster minikube
🔥  Creating hyperkit VM (CPUs=8, Memory=8192MB, Disk=20000MB) ...
🐳  Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Обращаем вниманием на слудеющую строку:  

```
🐳  Preparing Kubernetes v1.22.3 on Docker 20.10.8 ...
```

Мы видим что версия Kubernetes v1.22.3 и Docker 20.10.8 

# Ну-с, приступим!  

Наша цель:  развернуть Prometheus в кластере и замониторить все до чего дотянемся  

Для начала развернем сам Prometheus используя его Deployment  
Обратите внимание, "пустой" Prometheus нам не нужен  
По этому мы так его настроим путем добавления для него кастомного конфига  
Для этого мы воспользуемся объектом типа Configmap  

Для деплоя Prometheus воспользуемся следующими командами:  

```
# Создадим отдельный неймспейс
kubectl apply -f prom/01-ns.yaml
# Создадим Configmap для Prometheus
kubectl apply -f prom/03-config-map.yaml
# Создадим Deployment для Prometheus
kubectl apply -f prom/04-prometheus-deployment.yaml
# Создадим Service для Prometheus
kubectl apply -f prom/05-prometheus-service.yaml
```

Выполнив команду  
```
# Посмотреть поды в неймспейсе
kubectl -n monitoring get po
```

Ожидаем примерно следующий вывод  
```
NAME                                     READY   STATUS    RESTARTS   AGE
prometheus-deployment-599bbd9457-8z65q   1/1     Running   0          10m
```

```
# Посмотреть сервисы в неймспейсе
kubectl -n monitoring get svc
```

Ожидаем примерно следующий вывод 
```
NAME                 TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
prometheus-service   NodePort   10.99.212.162   <none>        8080:30000/TCP   3m17s
```

Открой prometheus в браузере, для этого "пробросим" порт себе локально  
```
kubectl -n monitoring port-forward svc/prometheus-service 8080:8080
```

Ожидаем примерно следующий вывод 
```
Forwarding from 127.0.0.1:8080 -> 9090
Forwarding from [::1]:8080 -> 9090
Handling connection for 8080
Handling connection for 8080
Handling connection for 8080
Handling connection for 8080
```

Открываем свой любимы браузер и переходим по ссылке  
```
http://localhost:8080/
```

Начинаем наше прекрасное путешествие по интерфейсу Prometheus и видим что он ничего не собирает 😭😭😭  

Потому что ему недостаточно прав, что бы решить эту проблему мы создадим Clusterrole и Clusterrolebinding  

```
kubectl apply -f prom/02-cluster-role.yaml
```

Снова проверяем что собирает Prometheus, и снова ничего нет 😭😭😭  

Нам нужен пак дполнительных компонентов:  

* Kube-state-metrics  
* Node-exporter  

Установить мы их можем из нужных директорий выполнив анлогичные команды ^_^  

Снова открываем наш интерфейс и вот оно счастье 😎

Теперь мы собираем метрики, но работать с ними не так что бы и очень удобно  
Установим Grafana  
Для этого применим манифесты из нужно директории  

Снова проверим наши поды и найдем там Grafana  

```
# Посмотреть сервисы в поды
kubectl -n monitoring get po
```

Постоянно пробрасывать себе порт не удобно, и не очень логично  
Созданим Ingress`ы для наших приложений 😎  
Так мы используем minikube, нам нужно включить addon для Ingress  
Сделать это можно следующей командой:  
```
minikube addons enable ingress
```  

Вывод примерно следующий:  
```
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v1.0.4
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled
```

Нам нужно узнать адрес нашей ВМ, для этого выполним команду  
```
minikube ip
```

Запомните адрес, он нам еще пригодится 😼  

Далее, мы добавляем Service с типом ClusterIP для Prometheus и Grafana  

После этого для каждого вновь добавленного сервиса мы добавляем Ingress  

Вот тут нам и понадобиться IP адрес который мы получили ранее 😎 

Теперь все наши компоненты доступны по адресам:  
```
grafana.192.168.64.23.nip.io
prometheus.192.168.64.23.nip.io
```